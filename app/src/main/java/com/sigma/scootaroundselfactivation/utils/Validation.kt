package com.sigma.scootaroundselfactivation.utils

import android.widget.EditText
import java.util.regex.Matcher
import java.util.regex.Pattern

class Validation {
    private val USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$"
    private lateinit var pattern: Pattern
    private lateinit var matcher: Matcher

    fun isEmpty(editText: EditText): Boolean {
        return if (editText.text.toString().length == 0 || editText.text.toString()
                .trim { it <= ' ' }
                .equals("", ignoreCase = true)
        ) true else false
    }

    fun isValidEmailId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    fun isConfirmpassword(pass: EditText, confirm: EditText): Boolean {
        return if (pass.text.toString().trim { it <= ' ' } == confirm.text.toString()
                .trim { it <= ' ' }) false else true
    }

    fun checkPasswordLength(pass: EditText): Boolean {
        return if (pass.text.toString().trim { it <= ' ' }.length >= 8) false else true
    }

    fun checkContactLength(contact: EditText): Boolean {
        return if (contact.text.toString().trim { it <= ' ' }.length < 5 || contact.text.toString()
                .trim { it <= ' ' }.length > 15
        ) true else false
    }

    fun Usernamematch(username: String?): Boolean {
        pattern = Pattern.compile(USERNAME_PATTERN)
        matcher = pattern.matcher(username)
        return matcher.matches()
    }
}