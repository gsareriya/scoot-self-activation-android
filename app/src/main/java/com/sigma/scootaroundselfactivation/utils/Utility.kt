package com.sigma.scootaroundselfactivation.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.Image
import android.net.ConnectivityManager
import android.text.Html
import android.util.Base64.NO_WRAP
import android.util.Base64.encodeToString
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.sigma.scootaroundselfactivation.R
import de.hdodenhof.circleimageview.CircleImageView
import java.io.UnsupportedEncodingException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object Utility {
    lateinit var dialog: Dialog

    fun showToast(context: Context, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun isNetworkConnected(context: Context): Boolean {
        if (context != null) {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }
        return true
    }

    fun showProgressDialog(context: Context?, canclable: Boolean?) {
        try {
            dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.progress_layout)
            dialog.setCancelable(canclable!!)
            Objects.requireNonNull(dialog.getWindow())
                ?.setBackgroundDrawable(
                    ColorDrawable(
                        Color.TRANSPARENT
                    )
                )

            dialog.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun _dialog(context: Context?, canclable: Boolean?,drawable_: Drawable?, title_:String , text_:String) {
        try {
            dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.connection_layout)
            dialog.setCancelable(canclable!!)
            val icon = dialog.findViewById<CircleImageView>(R.id.icon)
            val title = dialog.findViewById(R.id.title) as TextView
            val text = dialog.findViewById(R.id.text) as TextView
            val continues = dialog.findViewById(R.id.continues) as Button

            icon.setImageDrawable(drawable_)
            title.text = title_
            text.text = text_

            continues.setOnClickListener { dialog.dismiss() }

            dialog.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissProgressDialog(activity: Activity?) {
        try {
            if (dialog.isShowing() && activity != null && !activity.isFinishing) {
                dialog.dismiss()
            }
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun changeDateFormat(inputDateStr: String): String {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("MMMM dd, yyyy")
        val date: Date = inputFormat.parse(inputDateStr)
        val outputDateStr: String = outputFormat.format(date)
        return outputDateStr
    }
}