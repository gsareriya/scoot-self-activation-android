package com.sigma.scootaroundselfactivation.utils

import android.content.Context
import android.content.SharedPreferences

class AppPreference {

    private var sharedPreferences: SharedPreferences? = null

    constructor(context: Context) {
        sharedPreferences = context.getSharedPreferences("AppPreference", Context.MODE_PRIVATE)
    }

    fun setProfileModel(value: String?) {
        val prefsEditor = sharedPreferences!!.edit()
        prefsEditor.putString("profile_model", value)
        prefsEditor.commit()
    }

    fun getProfileModel(): String? {
        return if (sharedPreferences != null) {
            sharedPreferences!!.getString("profile_model", "")
        } else ""
    }

    fun setLocationId(value: Int) {
        val prefsEditor = sharedPreferences!!.edit()
        prefsEditor.putInt("location_id", value)
        prefsEditor.commit()
    }

    fun getLocationId(): Int {
        return if (sharedPreferences != null) {
            sharedPreferences!!.getInt("location_id", 0)
        } else 0
    }

    fun setLogin(value: Boolean) {
        val prefsEditor = sharedPreferences!!.edit()
        prefsEditor.putBoolean("login", value)
        prefsEditor.commit()
    }

    fun isLogin(): Boolean {
        return if (sharedPreferences != null) {
            sharedPreferences!!.getBoolean("login", false)
        } else false
    }
}