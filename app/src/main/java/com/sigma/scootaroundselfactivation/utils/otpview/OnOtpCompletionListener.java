package com.sigma.scootaroundselfactivation.utils.otpview;

public interface OnOtpCompletionListener {
  void onOtpCompleted(String otp);

}
