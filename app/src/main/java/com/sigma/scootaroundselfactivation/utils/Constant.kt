package com.sigma.scootaroundselfactivation.utils

object Constant {
    const val Order_History = "order_history"
    const val UNFULFILLED = "'UNFULFILLED'"
    const val UNFULFILLED_STATUS = "UNFULFILLED"
    const val ACTIVE_STATUS = "ACTIVE"
    const val COMPLETE_STATUS = "COMPLETE"
    const val ACTIVE = "'ACTIVE'"
    const val Delivery = "DEL"
    const val RETURN = "PU"
    const val COMPLETE = "'COMPLETE'"
    const val QuoteId = "quote_id"
    const val CustomerId = "customer_id"
    const val RentalId = "rental_id"
    const val api_url = "api/user.php"
    const val api_url_order = "api/order.php"
    const val BASE_URL = "http://kiosk.sigmasolve.net/"
    const val BASE_URL_TRACKABOUT = "https://test.trackabout.com/api/" // test url
    const val Image_url = BASE_URL+"image.php?txtDocumentID="
    const val Unit_Live_Url = "https://scootgp2.gosoftware.co/"+"image.php?txtDocumentID="
}