package com.sigma.scootaroundselfactivation.utils

object ApiTag {
    const val login = "login"
    const val forgot_password = "forgot password"
    const val confirm_otp = "confirm otp"
    const val change_password = "change password"
    const val logout = "logout"
    const val edit_profile = "edit profile"
    const val get_kiosk_code = "get kiosk code"
    const val user_location = "user location"
    const val gs_fetch_location_information = "gs fetch location information"
    const val gs_select_location = "gs select location"
    const val view_orders = "view orders"
    const val order_details = "order details"
    const val cancel_order = "cancel order"
    const val skip_order = "skip id scan"
    const val summary_order = "order details"
    const val refund_order = "retrieve refund reason list"
    const val add_flag_status = "add flag status"
    const val apply_refund = "refund order"
    const val deliveries = "deliveries"
    const val unit_count = "unit count"
    const val update_status = "update order status"

    const val swap_item_serial = "switch order"
    const val switch_order = "swap item serial"

    const val Scan_in_one = "scan in order id"
    const val Scan_out_one = "save scan details"
    const val Scan_out_two = "scan in scan out complete"

    const val API_USER_NAME="scootaroundwebservice"
    const val API_PASSWORD= "!`^TcSC(L7dbhE7\$"
    const val API_Key= "a1bcfed4-9d3c-4b1e-a0e2-8d10cfaf8efc" // test key
    const val authorization = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NTkwMzIzMTYsImV4cCI6MTg3NDM5MjMxNiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDg5IiwiYXVkIjpbImh0dHA6Ly9sb2NhbGhvc3Q6ODA4OS9yZXNvdXJjZXMiLCJub3BfYXBpIl0sImNsaWVudF9pZCI6ImQyZTM5OWQ2LWNiMTItNGQzNC05M2YxLWE1ZGM4ZmE5Mzk1NyIsInN1YiI6ImQyZTM5OWQ2LWNiMTItNGQzNC05M2YxLWE1ZGM4ZmE5Mzk1NyIsImF1dGhfdGltZSI6MTU1OTAzMjMxNiwiaWRwIjoibG9jYWwiLCJzY29wZSI6WyJub3BfYXBpIiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbInB3ZCJdfQ.X5ttG5UMW5w0h_RkFh3wPF8JRsa2fyamWcpSvwsu1V1HccDFliSu70hD2QwtJnQrpqEigOwRcbLLrNYhrjSp49zi8XhSEKz5OivDdUJPiuzcewLjoX4HNZssPFQnBmZsPqoAY1gMrqIWOv3N8QDohEQ2HBjgWi7JkfqiLuaPwtYaQxThw9NRa3h3a5zpkNj3C3_wNOOU69D-ZHmFp5kVEOCgnyYv4OcnYnnxod2Tlcp0X-oNcj-SkKoMhldFUpWtmp02QmasJIexgjCWK1u7ZSGH_uhx3MGP_pa6JDrcO_V6NLBm1MWhuTwRuDeiXaGH4BjhHx_mOBRcEqiQwIvJlA"
}