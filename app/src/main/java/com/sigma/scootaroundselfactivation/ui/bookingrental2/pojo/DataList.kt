package com.sigma.scootaroundselfactivation.ui.bookingrental2.pojo

import com.sigma.scootaround.`interface`.TypeCastModel

data class DataList(
    val title: String,
    val desc: String,
    val amount: String,
    val image: String
) : TypeCastModel()
