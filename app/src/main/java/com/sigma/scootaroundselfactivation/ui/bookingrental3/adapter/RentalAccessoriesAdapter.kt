package com.sigma.scootaroundselfactivation.ui.bookingrental3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.ui.bookingrental3.pojo.DataList

class RentalAccessoriesAdapter(
    private val context: Context,
    private val postItems_list: List<DataList>
) : RecyclerView.Adapter<RentalAccessoriesAdapter.ViewHolder>() {

    var tracker: SelectionTracker<Long>? = null

    init {
        setHasStableIds(true)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val number = postItems_list[position]
        tracker?.let {
            holder.bind(number, it.isSelected(position.toLong()))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.custom_rental_multi_select_unit, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return postItems_list.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = itemView.findViewById(R.id.title)
        val amount: TextView = itemView.findViewById(R.id.amount)

        fun bind(dataList: DataList, selected: Boolean) {
            title.setText(dataList.title)
            amount.setText(dataList.amount)
            itemView.isActivated = selected
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = adapterPosition
                override fun getSelectionKey(): Long? = itemId
            }
    }
}