package com.sigma.scootaroundselfactivation.ui.splash

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import android.widget.Toast
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.ui.home.Home_Screen
import com.sigma.scootaroundselfactivation.ui.splash.MangePermissions.ManagePermissions
import com.sigma.scootaroundselfactivation.utils.AppPreference

class Splash_Screen : AppCompatActivity() {

    private val PermissionsRequestCode = 123
    private lateinit var managePermissions: ManagePermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val list = listOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE
        )

        // Initialize a new instance of ManagePermissions class
        managePermissions = ManagePermissions(this, list, PermissionsRequestCode)

        if (managePermissions.checkPermissions()) {
            Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
                override fun run() {
                    navigateToLoginScreen()
                    val intent = Intent(this@Splash_Screen, Home_Screen::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }, 3000)
        }

    }

    private fun navigateToLoginScreen() {
        if (AppPreference(this).isLogin()) {
//            val intent = Intent(this, HomeActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
//            startActivity(intent)
        } else {
            /* val intent = Intent(this, SignIn::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
             startActivity(intent)*/
        }
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PermissionsRequestCode -> {
                val isPermissionsGranted = managePermissions.processPermissionsResult(
                    requestCode,
                    permissions,
                    grantResults
                )

                if (isPermissionsGranted) {
                    // Do the task now
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                        getWindow().setFlags(
                            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
                        )
                    }
                    Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
                        override fun run() {
                            navigateToLoginScreen()
                        }
                    }, 3000)
                } else {
                    toast("Permissions denied.")
                }
                return
            }
        }
    }


    // Extension function to show toast message
    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}