package com.sigma.scootaroundselfactivation.ui.bookingrental4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.ActivityRentalBookingFourBinding
import com.sigma.scootaroundselfactivation.databinding.ActivityRentalBookingOneBinding
import com.sigma.scootaroundselfactivation.databinding.ActivityRentalBookingThreeBinding

class Rental_booking_four : AppCompatActivity() {

    lateinit var binding: ActivityRentalBookingFourBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rental_booking_four)

        binding.swYesNo.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                binding.tvYes.text = getString(R.string.yes)
            } else {
                binding.tvYes.text = getString(R.string.no)
            }
        }
    }
}