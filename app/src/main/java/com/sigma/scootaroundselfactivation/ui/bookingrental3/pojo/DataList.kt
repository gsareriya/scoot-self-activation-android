package com.sigma.scootaroundselfactivation.ui.bookingrental3.pojo

import android.os.Parcelable
import com.sigma.scootaround.`interface`.TypeCastModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataList(
    val title: String,
    val desc: String,
    val amount: String,
    val image: String
) : Parcelable
