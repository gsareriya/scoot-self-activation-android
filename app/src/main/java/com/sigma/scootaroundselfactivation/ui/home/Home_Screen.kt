package com.sigma.scootaroundselfactivation.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.ActivityHomeBinding
import com.sigma.scootaroundselfactivation.ui.drawer.Dashboard
import com.sigma.scootaroundselfactivation.ui.otplogin.Otp_Login

class Home_Screen : AppCompatActivity() {

    lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        binding.go.setOnClickListener {
            val intent = Intent(this@Home_Screen, Dashboard::class.java)
            startActivity(intent)
        }
    }
}