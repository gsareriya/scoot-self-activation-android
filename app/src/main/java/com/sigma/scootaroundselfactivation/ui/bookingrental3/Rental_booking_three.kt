package com.sigma.scootaroundselfactivation.ui.bookingrental3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.view.ActionMode
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.ActivityRentalBookingThreeBinding
import com.sigma.scootaroundselfactivation.ui.bookingrental3.adapter.MyItemDetailsLookup
import com.sigma.scootaroundselfactivation.ui.bookingrental3.adapter.RentalAccessoriesAdapter
import com.sigma.scootaroundselfactivation.ui.bookingrental3.pojo.DataList
import com.sigma.scootaroundselfactivation.ui.bookingrental4.Rental_booking_four

class Rental_booking_three : AppCompatActivity() {

    lateinit var binding: ActivityRentalBookingThreeBinding
    lateinit var adapter: RentalAccessoriesAdapter

    ////////////////////////////////////////////////////////////////Temp
    private var tracker: SelectionTracker<Long>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rental_booking_three)

        binding.continues.setOnClickListener {
            val intent = Intent(this@Rental_booking_three, Rental_booking_four::class.java)
            startActivity(intent)
        }

        val datalist: MutableList<DataList> = mutableListOf()
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )
        datalist.add(
            DataList(
                "Cane Holder",
                "",
                "$5 USD per day",
                ""
            )
        )

        val mLayoutManager: LinearLayoutManager = GridLayoutManager(this@Rental_booking_three, 2)
        binding.rvRentalUnit.setLayoutManager(mLayoutManager)
        adapter = RentalAccessoriesAdapter(this@Rental_booking_three, datalist)
        binding.rvRentalUnit.adapter = adapter
        adapter.notifyDataSetChanged()

        tracker = SelectionTracker.Builder<Long>(
            "mySelection",
            binding.rvRentalUnit,
            StableIdKeyProvider(binding.rvRentalUnit),
            MyItemDetailsLookup(binding.rvRentalUnit),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate(
            SelectionPredicates.createSelectAnything()
        ).build()

        adapter.tracker = tracker
    }
}