package com.sigma.scootaroundselfactivation.ui.drawer.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.CustomListOrderBinding
import com.sigma.scootaroundselfactivation.databinding.ItemLoadingBinding
import com.sigma.scootaroundselfactivation.ui.drawer.Dashboard
import com.sigma.scootaroundselfactivation.ui.drawer.pojo.DataList

class LocationListAdapter(
    context: Dashboard,
    private var dataList: ArrayList<DataList>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var context: Context = context
    val VIEW_TYPE_LOADING = 0
    val VIEW_TYPE_NORMAL = 1
    private var isLoaderVisible = false

    class ViewHolder(binding: CustomListOrderBinding) :
        RecyclerView.ViewHolder(binding.getRoot()) {
        var binding: CustomListOrderBinding

        init {
            this.binding = binding
        }
    }

    class viewHolderLoader(binding: ItemLoadingBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        var binding: CustomListOrderBinding? = null
        when (viewType) {
            VIEW_TYPE_NORMAL -> {
                binding =
                    DataBindingUtil.inflate<CustomListOrderBinding>(
                        layoutInflater,
                        R.layout.custom_list_order,
                        parent,
                        false
                    )

                return ViewHolder(binding)
            }
            VIEW_TYPE_LOADING -> {
                val item_binding: ItemLoadingBinding =
                    DataBindingUtil.inflate<ItemLoadingBinding>(
                        layoutInflater,
                        R.layout.item_loading,
                        parent,
                        false
                    )

                return viewHolderLoader(item_binding)
            }
            else -> {
                return ViewHolder(binding!!)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder1: RecyclerView.ViewHolder, position: Int) {
        if (holder1.itemViewType == VIEW_TYPE_NORMAL) {
            val holder: ViewHolder = holder1 as ViewHolder
            var model = dataList.get(position)

            holder.binding.title.setText(model.name)

           /* Glide
                .with(context)
                .load(model.image)
                .placeholder(R.drawable.bg_otp_item)
                .into(holder.binding.ivImage)*/

        }
    }

    fun add(response: DataList) {
        dataList.add(response)
        notifyDataSetChanged()
    }

    fun removeLoading() {
        isLoaderVisible = false
        val position: Int = dataList.size - 1
        if (position > -1) {
            val item: DataList = getItem(position)
            if (item != null) {
                dataList.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    fun getItem(position: Int): DataList {
        return dataList.get(position)
    }

    override fun getItemViewType(position: Int): Int {
        Log.e("getItemViewType", position.toString())
        return if (isLoaderVisible) {
            if (position == dataList.size - 1) {
                VIEW_TYPE_LOADING
            } else {
                VIEW_TYPE_NORMAL
            }
        } else {
            VIEW_TYPE_NORMAL
        }
    }
}