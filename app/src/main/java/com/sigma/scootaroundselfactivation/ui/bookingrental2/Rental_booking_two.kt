package com.sigma.scootaroundselfactivation.ui.bookingrental2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.ActivityRentalBookingTwoBinding
import com.sigma.scootaroundselfactivation.ui.bookingrental2.adapter.RentalListAdapter
import com.sigma.scootaroundselfactivation.ui.drawer.adapter.LocationListAdapter
import com.sigma.scootaroundselfactivation.ui.bookingrental2.pojo.DataList
import com.sigma.scootaroundselfactivation.ui.bookingrental3.Rental_booking_three

class Rental_booking_two : AppCompatActivity() {

    lateinit var binding: ActivityRentalBookingTwoBinding
    lateinit var adapter: RentalListAdapter

    ////////////////////////////////////////////////////////////////Temp
    lateinit var datalist: ArrayList<DataList>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rental_booking_two)

        binding.continues.setOnClickListener {
            val intent = Intent(this@Rental_booking_two, Rental_booking_two::class.java)
            startActivity(intent)
        }

        datalist = ArrayList()

        val ds: DataList = DataList(
            "Standard Scooter",
            "3-Wheel Max Rider Weight: 450lbs.",
            "$40 USD per day",
            ""
        )
        datalist.add(0, ds)
        datalist.add(1, ds)
        datalist.add(2, ds)
        datalist.add(3, ds)
        datalist.add(4, ds)

        val mLayoutManager: LinearLayoutManager = GridLayoutManager(this@Rental_booking_two, 2)
        binding.rvRentalUnit.setLayoutManager(mLayoutManager)
        adapter = RentalListAdapter(this@Rental_booking_two, datalist)
        binding.rvRentalUnit.adapter = adapter

        binding.continues.setOnClickListener {
            val intent = Intent(this@Rental_booking_two, Rental_booking_three::class.java)
            startActivity(intent)
        }
    }
}