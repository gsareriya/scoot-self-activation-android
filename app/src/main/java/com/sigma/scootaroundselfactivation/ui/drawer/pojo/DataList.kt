package com.sigma.scootaroundselfactivation.ui.drawer.pojo

import com.sigma.scootaround.`interface`.TypeCastModel

data class DataList(
    val name: String,
    val image: String
) : TypeCastModel()
