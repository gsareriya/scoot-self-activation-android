package com.sigma.scootaroundselfactivation.ui.bookingrental1

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.DataBindingUtil
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.ActivityRentalBookingOneBinding
import com.sigma.scootaroundselfactivation.ui.bookingrental2.Rental_booking_two
import com.sigma.scootaroundselfactivation.ui.drawer.pojo.DataList
import java.text.SimpleDateFormat
import java.util.*

class Rental_booking_one : AppCompatActivity() {

    lateinit var binding: ActivityRentalBookingOneBinding
    var cal = Calendar.getInstance()

    ////////////////////////////////////////////////////////////////Temp
    val a = arrayOf("Event")
    val aa = arrayOf(
        "Orange County Airport Dunn Road, Montgomery, NY, USA",
        "Orange County Community College (SUNY Orange) South Street, M...",
        "Orange Caribbean restaurant and grill Flatlands Avenue, Brooklyn, NY, ...",
        "Orange County Convention Center International Drive, Orlando, FL, USA ...",
        "Orange County Convention Center Universal Boulevard, Orlando, FL, USA"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rental_booking_one)

        val delivery_set = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "MM/dd/yyyy"
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                binding.deliveryDate.text = sdf.format(cal.getTime())
            }
        }

        val return_set = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "MM/dd/yyyy"
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                binding.deliveryDate.text = sdf.format(cal.getTime())
            }
        }

        binding.deliveryDate.setOnClickListener {
            DatePickerDialog(
                this@Rental_booking_one,
                delivery_set,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        binding.deliveryTime.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                binding.deliveryTime.text = "By " + SimpleDateFormat("hh:mm a").format(cal.time)
            }
            TimePickerDialog(
                this,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                false
            ).show()
        }

        binding.returnDate.setOnClickListener {
            DatePickerDialog(
                this@Rental_booking_one,
                return_set,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        binding.returnTime.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                binding.returnTime.text = "By " + SimpleDateFormat("hh:mm a").format(cal.time)
            }
            TimePickerDialog(
                this,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                false
            ).show()
        }

        val adapter_sp = ArrayAdapter(this, android.R.layout.simple_spinner_item, a)
        binding.spRentalType.adapter = adapter_sp

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, aa)
        binding.autoCompleteTextView.setAdapter(adapter)

        binding.continues.setOnClickListener {
            val intent = Intent(this@Rental_booking_one, Rental_booking_two::class.java)
            startActivity(intent)
        }
    }
}