package com.sigma.scootaroundselfactivation.ui.drawer

import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.sigma.scootaroundselfactivation.R
import com.sigma.scootaroundselfactivation.databinding.ActivityDashboardBinding
import com.sigma.scootaroundselfactivation.ui.bookingrental1.Rental_booking_one
import com.sigma.scootaroundselfactivation.ui.drawer.adapter.LocationListAdapter
import com.sigma.scootaroundselfactivation.ui.drawer.pojo.DataList
import com.sigma.scootaroundselfactivation.utils.Utility

class Dashboard : AppCompatActivity() {

    lateinit var binding: ActivityDashboardBinding
    private var doubleBackToExitPressedOnce = false
    lateinit var adapter: LocationListAdapter
    lateinit var navView: NavigationView

    ////////////////////////////////////////////////////////////////Temp
    lateinit var datalist: ArrayList<DataList>

    val a = arrayOf("Event")
    val b = arrayOf("Orange County Convention Center")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        init()
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    private fun init() {
        navView = findViewById(R.id.nav_view)
        datalist = ArrayList()

        getrentaltype()

        binding.includeMainLayout.ivDrawer.setOnClickListener {
            opendrawer()
        }

        val ds: DataList = DataList("Disney Magic Kindom", "")
        datalist.add(0, ds)
        datalist.add(1, ds)
        datalist.add(2, ds)
        datalist.add(3, ds)
        datalist.add(4, ds)

        val mLayoutManager: LinearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.includeMainLayout.rvLocNearMe.setLayoutManager(mLayoutManager)
        adapter = LocationListAdapter(this, datalist)
        binding.includeMainLayout.rvLocNearMe.adapter = adapter

        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            binding.tvVersion.text = ("v" + version)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        binding.includeMainLayout.btGo.setOnClickListener {
            val intent = Intent(this@Dashboard, Rental_booking_one::class.java)
            startActivity(intent)
        }
    }

    private fun getrentaltype() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, a)
        binding.includeMainLayout.spRentalType.adapter = adapter
        val adapter2 = ArrayAdapter(this, android.R.layout.simple_spinner_item, b)
        binding.includeMainLayout.spDeliveryLocation.adapter = adapter2
    }

    fun closeDrawer() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    fun opendrawer() {
        binding.drawerLayout.openDrawer(GravityCompat.START)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_main_drawer, menu)

        navView.getMenu().getItem(4).setActionView(R.layout.menu_nav_icon);
        navView.getMenu().getItem(5).setActionView(R.layout.menu_nav_icon);
        navView.getMenu().getItem(6).setActionView(R.layout.menu_nav_icon);
        navView.getMenu().getItem(7).setActionView(R.layout.menu_nav_icon);

        return true
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }

            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
            Utility._dialog(
                this,
                false,
                resources.getDrawable(R.drawable.ic_questionmark),
                "Where are you going?",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce felis tellus, feugiat sed bibendum non, lacinia auctor ligula. Donec consectetur, urna ac mollis volutpat, justo mauris accumsan lacus, eget varius magna nisi ut mauris. Sed vel tempor ante, vel gravida ante. Nulla a nulla aliquam, iaculis lectus non, malesuada eros."
            )
            Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)

        }
    }
}