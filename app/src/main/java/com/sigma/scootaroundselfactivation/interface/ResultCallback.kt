package com.sigma.scootaround.`interface`

interface ResultCallback {

    fun onSuccess(model: TypeCastModel, apiTag: String)
    fun onFailure(message: String, apiTag: String)
}